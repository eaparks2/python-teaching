import numpy as np
import scipy as sp
import random

class human:

    def __init__(self, firstName, middleName, lastName, father, mother, height, weight, eyeColor, ethnicity, favColor, age):
        self.firstName = firstName
        self.middleName = middleName
        self.lastName = lastName
        self.father = father
        self.mother = mother
        self.height = height
        self.weight = weight
        self.weightKG = weight * 0.453592
        self.eyeColor = eyeColor
        self.ethnicity = ethnicity
        self.favColor = favColor
        self.age = age

    def birthday(self):
        self.age += 1

class family:

    def __init__(self, familyName, husband, wife):
        self.familyName = familyName
        self.husband = husband
        self.wife = wife
        self.children = []
        self.grandchildren = []

    def baby(self, firstName, middleName, height, weight, eyeColor, ethnicity, favColor, isGrandchild, lastName):
        if (isGrandchild == 0):
            newChild = human(firstName, middleName, self.familyName, self.husband.firstName, self.wife.firstName, height, weight, eyeColor, ethnicity, favColor, 0)
            self.children.append(newChild)
        else:
            newChild = human(firstName, middleName, lastName, self.husband.firstName, self.wife.firstName, height, weight, eyeColor, ethnicity, favColor, 0)
            self.grandchildren.append(newChild)

def main():
    patriarch = human("Eric","Anthony","Parks","Patrick","Michelle",75,195,"Green","Croatian","Lime",21)
    matriarch = human("Rebecca","Anne","Parks","Michael","Kayla",68,135,"Blue","Italian","Navy",20)
    parksFam  = family("Parks",patriarch,matriarch)
    parksFam.baby("Maja","Lynn",11,8,"Green","Croatian-Italian","Lime",0,None)
    parksFam.baby("Kata","Le",10,7.5,"Blue","Croatian-Italian","Navy",0,None)
    randNum = random.random()
    if (randNum < 0.5):
        parksFam.baby("Ryan","Eugene",11,10,"Brown","Irish","Pink",0,None)
    else:
        parksFam.baby("Danijela","Patricija",11,10,"Green","Croatian","Purple",0,None)
    print(parksFam.husband.firstName)
    print(parksFam.wife.firstName)
    for index in range(len(parksFam.children)):
        print(parksFam.children[index].firstName)
    parksFam.baby("Monika","Danijela",11,8,"Green","Croatian-VERYItalian","Lime",1,"Surdo")
    for index in range(len(parksFam.grandchildren)):
        print(str(parksFam.grandchildren[index].firstName) + " " + str(parksFam.grandchildren[index].lastName))
    print(parksFam.children[0].weightKG)
    for index in range(21):
        parksFam.children[0].birthday()
    print(parksFam.children[0].age)

if __name__ == ("__main__"):
    main()
